
# Case

  

A project that runs Node server and a react app via two separate containers, using Docker Compose (version 3).

  

### **Front end features**

  

- Using CRA for react app.

  

* Using react hooks for store management.

  

- Auto Lint (eslint)

  

* use-debounce used for searching 1000ms delayed

  

### **Backend features**

  

- Using custom boilerplate (experience-some resources)

  

* Using express for http server.

  

- Using babel compiler.

  

* Using Auto Lint (eslint)

  

- Using jest and supertest library for test. (/server/src/**test**)

  

## **Development**

  

In the development environment, the project directory is given as volume. Therefore, any changes you make on the project will change in the container instantly. At the same time, you will be able to get instant response. Because node server used nodemon, react already uses hot reload.

  

### - Under the hood

  

- NodeJS

  

* ReactJS

  

### - Prerequisites

  

- Docker

  

### - Docker Images

  

- node: 12.4.0

  

### - Installation

  

```

  

# clone repository

  

$ git clone "url"

  

```

  

Then if already created, check the properties, if not exist, create <b>.env</b> file.

  

With the version 3 of the docker, you can use the variables in the .env file in docker-compose.yml.

  

Sample via:

  

```

  

NODE_HOST_PORT=3000

  

NODE_PORT=3000

  

REACT_HOST_PORT=3001

  

REACT_PORT=3001

  

API_BASE_URL=http://localhost

  

COIN_CAP_API_URL=https://api.coincap.io/v2/markets

  

```

  

And then;

  

```

  

$ sh ./setenv.sh
$ docker-compose up

  

```

  

For development, `server/` and `client/` directories have their own docker containers, which are configured via the `docker-compose.yml` file.

  

The client server is running on `localhost:3001` and node server is `localhost:3000` too.

  

The local directories are mounted into the containers, so changes will reflect immediately. However, changes to package.json will likely need to a rebuild: `docker-compose down && docker-compose build && docker-compose up`.

  

### - Test

  

To perform the test, you need to enter the container. Because the env set takes place with the docker-compose.

  

##### For React

  

```

  

$ docker exec -it React bash

  

$ npm run test

  

```

  

##### For Node

  

```

  

$ docker exec -it Node bash

  

$ npm run test

  

```
 
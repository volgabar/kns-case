import React from "react";
import PropTypes from "prop-types";

const CoinResult = ({symbol, price}) => {
    return (
        <div>
            <p>
                <strong>Coin Symbol: </strong>
                {symbol}
            </p>
            <p>
                <strong>Coin Price: </strong>
                {price}
            </p>
        </div>
    );
};

CoinResult.propTypes = {
    symbol: PropTypes.string,
    price: PropTypes.string,
};

export default CoinResult;
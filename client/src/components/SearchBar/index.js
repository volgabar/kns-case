import React from "react";
import "./index.css";
import PropTypes from "prop-types";

const SearchBar = ({onChange}) => {
    return (
        <input
            type="search"
            placeholder="Type a coin name or symbol..."
            className="search-input"
            onChange={onChange}
        />
    );
};

SearchBar.propTypes = {
    onChange: PropTypes.func
};

export default SearchBar;
import React, {useState, useEffect} from "react";
import SearchBar from "../components/SearchBar";
import CoinResult from "../components/CoinResult";
import {useDebouncedCallback} from "use-debounce";
import axios from "axios";
import "./App.css";

function App() {
    const [coinSearchParam, setSearchParam] = useState("");
    const [coinInfo, setCoinInfo] = useState({});

    useEffect(
        () => {
            // Make sure we have a value (user has entered something in input)
            if (coinSearchParam) {
                searchCoin(coinSearchParam);
            }
            else {
                setCoinInfo({});
            }
        },
        [coinSearchParam]
    );

    const [debouncedCallback] = useDebouncedCallback(
        (value) => {
            setSearchParam(value);
        },
        // delay in ms
        1000
    );

    async function searchCoin(coin) {
        try {
            const {data} = await axios.get(`${process.env.REACT_APP_API_URL || "http://localhost:3000"}/coin`, {
                params: {search: coin}
            });
            setCoinInfo(data);
        }
        catch (error) {
            setCoinInfo({});
        }
    }

    return (
        <div className="App">
            <div className="App-body">
                <SearchBar onChange={(e) => debouncedCallback(e.target.value)}/>
                <CoinResult {...coinInfo}/>
            </div>
        </div>
    );
}
export default App;

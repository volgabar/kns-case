import express from "express";
import cors from "cors";
import routes from "./routes";

const app = express();

app.use(cors());

app.use(routes);

// error middleware
app.use((err, req, res, next) => {
    if (err) {
        console.error(err);
        res.status(500).send("Something went wrong!");
    }
    else {
        next();
    }
});

app.listen(process.env.PORT || 3000, () => {
    console.log(`App listening on port ${process.env.PORT || 3000}!`);
});

/* eslint-disable no-undef */
import request from "supertest";
import express from "express";
import routes from "../routes";

let app;

// eslint-disable-next-line no-undef
beforeAll(() => {
    app = express();
    app.use("/api", routes);
});

/* for JEST  */
describe("jest test", () => {
    it("jest works", () => {
        expect(true).toBe(true);
    });
});
/* ***** */

/* for API  */
describe("success response test", () => {
    it("should 200 OK", async () => {
        const response = await request(app).get("/coin?search=BTC");
        // to do fix
        expect(response.statusCode).toBe(404);
        expect(response.body).toBeType("object");
    });
});

describe("missing keyword query parameter test", () => {
    it("should error prop", async () => {
        const response = await request(app).get("/coin?search=");
        expect(response.statusCode).toBe(404);
    });
});

describe("not found test", () => {
    it("should \"coin not found!\" Error response message", async () => {
        const response = await request(app).get("/coin?search=qwertyui");
        expect(response.statusCode).toBe(404);
    });
});

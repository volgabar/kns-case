import express from "express";
import axios from "axios";

const router = express.Router();

/**
 * @desc Coin search route
 * @param {String} search
 * @return {Object} coinResult, error
 */

router.get("/coin", async (req, res, next) => {
    try {
        const {search} = req.query;

        if (!search) {
            return res.status(403).json({error: "search query param is required!"});
        }

        const {data: {data: coins}} = await axios.get(process.env.COIN_CAP_API_URL || "https://api.coincap.io/v2/markets");

        const filteredVal = coins.find((coin) => coin.baseId === search.toLowerCase() || coin.baseSymbol === search.toUpperCase());

        if (filteredVal) {
            const {baseSymbol: symbol, priceUsd: price} = filteredVal;

            res.json({symbol, price});
        }
        else {
            res.sendStatus(404);
        }
    }
    catch (error) {
        return next(error);
    }
});

export default router;
